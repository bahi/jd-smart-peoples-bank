# JD智慧人行

<!-- markdownlint-disable MD033 -->


<!-- markdownlint-restore -->

## 官方网址

[👉👉👉点击进入](https://pk.jdd966.com/open-source-ui/#/)

## 项目简介

> JD智慧人行项目，以访客管理为核心，依托智能访客机和智慧通行管理系统，构建多场景覆盖的访客管理体系，有助于物业进行人员分类管理，更为访客打造智慧通行体验。
 应运而生，在现代社会管理中发挥着越来越重要的作用。
> JD智慧人行项目为前后端分离项目，前端是vue开发，后端java开发，应用现代机械电子及通讯科学技术，集控制硬件、软件于一体。并兼容市面90%的硬件设备，实现无人值守。
> 高效率，精准测温，快速识别健康码，无接触操作，降低交叉感染风险。测温设备联网，测温和健康码等数据支持实时上报，有利于早发现、早外理异常人员。
>数据互通，线下采集设备预数，移动端预整和平台端预整三重预机制，严密守护场所安全。门禁，登记信息，测温记录数据相互融合，一经发现数据异常情况，即可销定相关人员。
> 您无需关注底层基础设施及服务化相关的开发、治理和运维，即可高效、安全地对模型、引擎进行部署、升级、扩缩、运营和监控。
* 公司另发布另外一款开源项目：JD智慧停车[👉👉👉点击进入](https://gitee.com/jddhlw_1/park?_from=gitee_search)

## 整体架构

 ![image text](https://test.jdd966.cn/images/park-station/架构图.png)

## 特性

&#9745; 支持模型推理成RPC服务(Serving框架会转成HTTP服务)

&#9745; 支持Java代码推理

&#9745; 支持配置中心，服务发现

&#9745; 支持负载均衡配置

&#9745; 支持HTTP/GRPC服务


## 业务介绍

#### 开源系统主要包含功能：   
* 1.首页菜单：（退出登录，设备统计，通行统计，来访被访统计）
* 2.系统管理（用户管理，角色管理，项目管理，菜单授权，菜单管理，个人信息）
* 3.项目管理（项目区域，区域管理，项目列表，房间管理）
* 4.人员管理（部门组织，人员类型，人员列表）
* 5.记录明细（开门记录，通道记录）
* 6.设备管理（门禁设备，门状态配置）

#### 演示系统
[👉👉👉点击进入](https://test.jdd966.cn/jdd-people-passage-ui/#/user/login?redirect=%2Fdashboard%2Fanalysis)
* 用户名：SysAdmin
* 密码：Sys123456

## 安装说明

1.电脑配置标准:
 ```bash
处理器：i5及以上  内存：8G及以上   硬盘：500G及以上
 ```
2.需要配置软件环境
 ```bash
软件环境：JDK、nginx、MYSQL数据库等
 ```
3.数据库脚本
```bash
打开 jdd-people-passage 项目，进入src/resources目录下，将park_db.sql的脚本运行至本地数据库
 ```
4.启动项目
```bash
进入 jdd-people-passage 项目
运行项目 JddPeoplePassageApplication.Java
 ```
 ```bash
进入 jdd-people-passage-ui项目
安装依赖， yarn install
编译项目， yarn run build
运行项目， yarn run server
 ```
 ```bash
账户：test
密码：123456
 ```

## 开源及商务合作说明

#### 开源说明：
  
| 选择类型    | 免费版  | 技术支持版  | 商城源码版本   |
|---------|------|--------|----------|
| 费用      | 0元   | 1000元  | 5000元    |
| 期限      | 终生   | 终生     | 终生       |
| 二次开发并商用 | 可以   | 可以     | 可以       |
| 安装部署    | 自主安装 | 专业人员安装 | 提供源码自主安装 |
| bug修复   | 自主修复 | 专业人员修复 | 自主修复     |
| 技术支撑    | 自主研究 | 专业人员支撑 | 自主研究     |
| 业务培训    | 无    | 有      | 无        |
| 系统搬家    | 无    | 有      | 无        |
| 授权证明    | 无    | 有      | 有        |

* 您使用本产品二次开发的衍生产品，只能自用或出售别人使用，不能作为开源产品与本产品产生竞争。
* 如果你喜欢我们的产品, 可以放心使用, 在产品商业化的支持下同时我们会尽最大的努力维护开源版的稳定。
* 后期我们会逐步开放更多的信息化平台。在此期间,如果你发现产品有任何Bug, 请及时沟通联系我们，谢谢。

#### 商务合作：
本项目同时支持开源版实施和定制开发服务。可定制云端+场端部署模式。可支持数据分发上报各政府平台。

* 商务合作请统一联系 
* 刘经理：18539299635
* 邮箱：jindoudouparking@jdd966.com
* qq群:370451426



## 人行系统附图

#### 登录:
 ![image text](https://test.jdd966.cn/images/people-passage/登录.png)

#### 首页:
 ![image text](https://test.jdd966.cn/images/people-passage/首页.png)

#### 系统管理:
 ![image text](https://test.jdd966.cn/images/people-passage/系统管理1.png)

 ![image text](https://test.jdd966.cn/images/people-passage/系统管理2.png)

 ![image text](https://test.jdd966.cn/images/people-passage/系统管理3.png)
 
 ![image text](https://test.jdd966.cn/images/people-passage/系统管理4.png)

 ![image text](https://test.jdd966.cn/images/people-passage/系统管理5.png)
 
 ![image text](https://test.jdd966.cn/images/people-passage/系统管理6.png)
 

#### 项目管理:
 ![image text](https://test.jdd966.cn/images/people-passage/项目管理1.png)

 ![image text](https://test.jdd966.cn/images/people-passage/项目管理2.png)

 ![image text](https://test.jdd966.cn/images/people-passage/项目管理3.png)
 
 ![image text](https://test.jdd966.cn/images/people-passage/项目管理4.png)


#### 人员管理:
 ![image text](https://test.jdd966.cn/images/people-passage/人员管理1.png)

 ![image text](https://test.jdd966.cn/images/people-passage/人员管理2.png)

 ![image text](https://test.jdd966.cn/images/people-passage/人员管理3.png)


#### 记录明细:
 ![image text](https://test.jdd966.cn/images/people-passage/记录明细1.png)

 ![image text](https://test.jdd966.cn/images/people-passage/记录明细2.png)

 ![image text](https://test.jdd966.cn/images/people-passage/记录明细3.png)
 
 ![image text](https://test.jdd966.cn/images/people-passage/记录明细4.png)


#### 设备管理:
 ![image text](https://test.jdd966.cn/images/people-passage/设备管理1.png)

 ![image text](https://test.jdd966.cn/images/people-passage/设备管理2.png)

 ![image text](https://test.jdd966.cn/images/people-passage/设备管理3.png)


**注意备注来源: 开源** 



